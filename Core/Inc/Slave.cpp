/*
 * Slave.cpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#include "Slave.hpp"

namespace RoboCAN {

std::vector<Slave> slaveVec;

Slave::Slave(unsigned myId):
		myId(myId)
{
	RoboCAN::filterSettingSlave(myId);
}

unsigned Slave::getId()
{
	return myId;
}

} /* namespace RoboCAN */
