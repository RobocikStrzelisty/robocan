/*
 * CAN_Driver.cpp
 *
 *  Created on: 8 cze 2021
 *      Author: Dell
 */


#include "CAN_Driver.hpp"

extern RoboCAN::Master master;

namespace RoboCAN {

//namespace
//{
//	constexpr unsigned STD_ID_SIZE = 11U;
////	constexpr unsigned STD_ID_SIZE = 11U;
//}

//Message getMessage()
//{
//	CAN_RxHeaderTypeDef header;
//	// tablica (jednoelemntowa bo tyle potrzebujemy) na odbierane dane
//	uint8_t data[8] = {0};
//
//	// Odpalamy funkcję odczytującą wiadomość z danego fifo
//	HAL_CAN_GetRxMessage(&hcan, CAN_RX_FIFO0, &header, data);
//
//	unsigned masterId = header.StdId >> (STD_ID_SIZE - RoboCAN::MASTER_ID_SIZE);
//	unsigned slaveId = (header.StdId & 0x1F) << 5U | (header.ExtId >> 13U);
//	unsigned fncNumber = (header.ExtId >> 8U) & 0x1FU;
//
//	return Message(masterId, slaveId, fncNumber, data);
//}
Message getMessage(unsigned whichFifo) // podajemy numer fifo, z ktorego odbieramy wiadomosc
{
	CAN_RxHeaderTypeDef header;
	std::bitset<32> header_bitset;
	// tablica (jednoelemntowa bo tyle potrzebujemy) na odbierane dane
	uint8_t data[8] = {0};

	// Odpalamy funkcję odczytującą wiadomość z danego fifo
	HAL_CAN_GetRxMessage(&hcan, whichFifo, &header, data);


	header_bitset = (header.ExtId << 3U) | (1U << 2U) | (header.RTR);
	return Message(header_bitset, data, header.DLC);
}

CAN_TxHeaderTypeDef messageToHalHeader(const Message& msg)
{
	CAN_TxHeaderTypeDef header;
	unsigned targetId = msg.getMessageId(); // potem wywalic

	header.ExtId = targetId >> 3U;

	header.IDE = CAN_RI0R_IDE; // używamy ramek rozszerzonych, więc by gównofunkcje halowskie to dobrze interpretowały, to zamiast 1 trzeba tu wpisać 4 XDD
	header.DLC = msg.getData().size(); // po zmianie na arraya tu trzeba będzie zmienić
	header.RTR = targetId & CAN_RI0R_RTR; // jeśli jest zapalony bit RTR, czyli ramki mastera, to zapisz 4 do pola struktury - tak dziwnie, bo tak dziwne są te biblioteki halowskie
	return header; // chuj wie, czy to zadziała, czy mi nie zniknie po wyjściu z funkcji, najwyżej zmieni się, że zwraca kopię, a nie refa
}

void filterSettingSlave(unsigned myId)
{
	CAN_FilterTypeDef filter;

	filter.FilterIdHigh = myId;
	filter.FilterMaskIdHigh = (1U << 10U) - 1U; // interesuje nas 10 bitow slaveId - czy do nas
	filter.FilterIdLow = 2U; //w sumie nasza maska tego nie obejmuje, ale warto wyzerowac na przyszlosc
	filter.FilterMaskIdLow = 2U;
	filter.FilterFIFOAssignment = CAN_FilterFIFO0;
	filter.FilterBank = RoboCAN::slaveVec.size();
	filter.FilterMode = CAN_FILTERMODE_IDMASK;
	filter.FilterScale = CAN_FILTERSCALE_32BIT;
	filter.FilterActivation = CAN_FILTER_ENABLE;

	// Włączamy przerwanie od RxFifo0-wisząca wiadomość- na CANie, można to zrobić prosto zapisując odpowiedni
	// bit w rejestrze, albo zrobić to samo jakąś udziwnioną halowską funkcją
	//hcan.Instance->IER |= CAN_IER_FMPIE0;
	HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO0_MSG_PENDING);

	// uruchamiamy funkcję konfigurującą filtr
	HAL_CAN_ConfigFilter(&hcan, &filter);
}

void filterSettingMaster(unsigned masterId, unsigned slaveId)
{
	CAN_FilterTypeDef filter;

	filter.FilterIdHigh = slaveId | masterId << 10U;
	filter.FilterMaskIdHigh = 0xFFFF; // tutaj ffff bo masterId tez nas obchodzi
	filter.FilterIdLow = 0U; //w sumie i tak nasza maska tego nie obejmuje
	filter.FilterMaskIdLow = 2U; // master przyjmuje wiadomosci tylko od slavow
	filter.FilterFIFOAssignment = CAN_FilterFIFO1;
	filter.FilterBank = 5; // TO DO !!! powinno byc wybierane automatycznie
	filter.FilterMode = CAN_FILTERMODE_IDMASK;
	filter.FilterScale = CAN_FILTERSCALE_32BIT;
	filter.FilterActivation = CAN_FILTER_ENABLE;

	// Włączamy przerwanie od RxFifo0-wisząca wiadomość- na CANie, można to zrobić prosto zapisując odpowiedni
	// bit w rejestrze, albo zrobić to samo jakąś udziwnioną halowską funkcją
	//hcan.Instance->IER |= CAN_IER_FMPIE0;
	HAL_CAN_ActivateNotification(&hcan, CAN_IT_RX_FIFO1_MSG_PENDING);

	// uruchamiamy funkcję konfigurującą filtr
	HAL_CAN_ConfigFilter(&hcan, &filter);
}

void sendMessage(const Message& msg)
{
//	volatile unsigned xd = msg.getMessageId();
	CAN_TxHeaderTypeDef header = messageToHalHeader(msg);
//	volatile unsigned header_bitset = 0U | (header.StdId << 21) | (header.ExtId << 3) | (header.IDE << 2) | (header.RTR << 1);
	uint32_t _; // nie zapamiętujemy do ktorej skrzynki trafila wiadomosc, bo nie jest to nam potrzebne
	HAL_CAN_AddTxMessage(&hcan, &header, (uint8_t*)msg.getData().data(), &_);
}

// poniższa funkcja raczej robiona dla slave'ów, potem się obczai, czy dla masterów też się przyda
// i czy będzie potrzebować dodatkowych przystosowań
void sendData(void * data, unsigned size, const Message& msg) // size oczywiscie w bajtach
{
	uint8_t * _data = (uint8_t *)data;
	// początkowo robimy implementację blokującą
	while(size > 0)
	{
		while(HAL_CAN_GetTxMailboxesFreeLevel(&hcan) == 0); // czekamy na zwolnienie skrzyneczki
		unsigned msgSize = std::min(size, 8U);
		size -= msgSize;
		// xorujemy poniżej, by przełączyć bit RTR, czyli jeśli slave odpowiada masterowi, to
		const Message& newmsg = Message(msg.getMessageId() ^ CAN_RI0R_RTR, _data, msgSize);
		sendMessage(newmsg);
		_data += 8;
	}
}



// przerwanie dla slave
void USB_LP_CAN1_RX0_IRQHandler()
{
	// można dalej przyjąć, że master będzie korzystać zawsze z fifo1, slave z fifo0
	// jednak nie jest to konieczne - do rozpatrzenia
	Message msg = getMessage(CAN_RX_FIFO0);
	for(Slave slave : slaveVec)
	{
		if(slave.getId() == msg.getSlaveId())
		{
			slave.procedures[msg.getFunctionNumber()](msg);
			break; // nie wiadomo czy tak zostanie, zalezy od tego, czy beda mogly byc obiekty o tym samym id
		}
	}
}

// przerwanie dla master
void CAN1_RX1_IRQHandler(void)
{
	Message msg = getMessage(CAN_RX_FIFO1);
	for(Port port : master)
	{
		if(port.getId() == msg.getSlaveId())
		{
			port.onReceive(msg);
			break; // nie wiadomo czy tak zostanie, zalezy od tego, czy beda mogly byc obiekty o tym samym id
		}
	}
}

void NoSuchSlaveId_Error()
{
	Error_Handler();
}

}

void USB_LP_CAN1_RX0_IRQHandler() {RoboCAN::USB_LP_CAN1_RX0_IRQHandler();}
void CAN1_RX1_IRQHandler() {RoboCAN::CAN1_RX1_IRQHandler();}


