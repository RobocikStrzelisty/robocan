/*
 * CzujnikTwojejStarej.hpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#ifndef INC_CZUJNIKTWOJEJSTAREJ_HPP_
#define INC_CZUJNIKTWOJEJSTAREJ_HPP_

#include "RoboCAN.hpp"
#include "Message.hpp"

#include "stm32f1xx_ll_gpio.h"

class CzujnikTwojejStarej {
public:
	CzujnikTwojejStarej(int dupa = 2137, int id=0x12);

	void wlaczDiode(RoboCAN::Message& msg);
	void wylaczDiode(RoboCAN::Message& msg);
	void zapiszDane(RoboCAN::Message& msg);
	int dajDane(RoboCAN::Message& msg);

	int chuj;
	int idTwojejStarej; // obiekt wrzucany do slave'a bedzie musial zawierac to pole
};



#endif /* INC_CZUJNIKTWOJEJSTAREJ_HPP_ */
