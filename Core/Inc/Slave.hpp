/*
 * Slave.hpp
 *
 *  Created on: Jun 7, 2021
 *      Author: Dell
 */

#ifndef INC_SLAVE_HPP_
#define INC_SLAVE_HPP_

#include <vector>
#include <functional>
#include "Message.hpp"
#include "CAN_Driver.hpp"

namespace RoboCAN {

class Slave {
public:
	Slave(unsigned myId);

	unsigned getId();

	std::vector<std::function<void(Message&)>> procedures;

private:
	unsigned myId; // wspolna przestrzen adresowa slejwow dla calej sieci

//	std::string myName;
//	void giveName(void); rozwojowe, mozna to zaimplementowac, by np. komputer mogl poprzez kontroler usb dowiedziec sie
	// jakie obiekty sa w sieci
};

extern std::vector<Slave> slaveVec;

} /* namespace RoboCAN */

#endif /* INC_SLAVE_HPP_ */
